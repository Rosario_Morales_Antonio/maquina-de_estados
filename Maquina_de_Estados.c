#include <stdio.h>

typedef enum{
    goNORTE,
    waitNORTE,
	goESTE,
	waitESTE
}Estados;

typedef enum{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
}Entradas;



static int EstadoActualG = goNORTE;

void VerEstadoActual(int EdoActual){
	switch (EdoActual){
        case goNORTE:
            printf("goNORTE");
            break;
        case waitNORTE:
            printf("waitNORTE");
            break;
        case goESTE:
            printf("goESTE");
            break;
        case waitESTE:
            printf("waitESTE");
            break;
	}
};


void MaquinaDeEstados(int Transicion, int EstadoActual){
    switch (EstadoActual){
    case goNORTE:
            if (Transicion == NO_CARROS || Transicion == CARROS_NORTE){
		        EstadoActualG = goNORTE;
            }else{
		        EstadoActualG = waitNORTE;
            }
        break;
    
    case waitNORTE:
	        EstadoActualG = goESTE;
        break;

    case goESTE:
            if (Transicion == NO_CARROS || Transicion == CARROS_ESTE){
		        EstadoActualG = goESTE;
            }else{
		        EstadoActualG = waitESTE;
            }
        break;
    
    case waitESTE:
	        EstadoActualG = goNORTE;
        break;
    }
    VerEstadoActual(EstadoActualG);
};


int main(){
    int ListaTransiciones[6] ={
        CARROS_ESTE, 
        CARROS_ESTE, 
        CARROS_NORTE, 
        AMBOS_LADOS,
        NO_CARROS,
        AMBOS_LADOS
    };

	printf("\nInicio [");
    VerEstadoActual(EstadoActualG);
    printf("]");
    
    for (int i = 0; i < 6; i++){
	    printf("\nEstado Actual [");
        MaquinaDeEstados(ListaTransiciones[i], EstadoActualG);
        printf("]");
    }
    printf("\n\n");
    return 0;    
}
