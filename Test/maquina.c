#include <stdio.h>

typedef enum  
{
    goNORTE,
    waitNORTE,
	goESTE,
	waitESTE
}Estados;

typedef enum 
{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
}Entradas;

static int EstadoActualG = 0;

int MaquinaDeEstados(int Transicion, int EstadoActual){
    switch (EstadoActual){
    case goNORTE:
            if (Transicion == NO_CARROS || Transicion == CARROS_NORTE){
		        EstadoActualG = goNORTE;
            }else{
		        EstadoActualG = waitNORTE;
            }
        break;
    
    case waitNORTE:
	        EstadoActualG = goESTE;
        break;

    case goESTE:
            if (Transicion == NO_CARROS || Transicion == CARROS_ESTE){
		        EstadoActualG = goESTE;
            }else{
		        EstadoActualG = waitESTE;
            }
        break;
    
    case waitESTE:
	        EstadoActualG = goNORTE;
        break;
    }
    return EstadoActualG;
};

