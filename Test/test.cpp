#include "maquina.c"
#include <gtest/gtest.h>

TEST(State0, Machine0){
    ASSERT_EQ(goNORTE, MaquinaDeEstados(NO_CARROS,goNORTE));
}
TEST(State1, Machine1){
    ASSERT_EQ(goNORTE, MaquinaDeEstados(CARROS_NORTE,goNORTE));
}
TEST(State2, Machine2){
    ASSERT_EQ(waitNORTE, MaquinaDeEstados(CARROS_ESTE,goNORTE));
}
TEST(State3, Machine3){
    ASSERT_EQ(waitNORTE, MaquinaDeEstados(AMBOS_LADOS,goNORTE));
}


TEST(State4, Machine4){
    ASSERT_EQ(goESTE, MaquinaDeEstados(NO_CARROS,waitNORTE));
}
TEST(State5, Machine5){
    ASSERT_EQ(goESTE, MaquinaDeEstados(CARROS_ESTE,waitNORTE));
}
TEST(State6, Machine6){
    ASSERT_EQ(goESTE, MaquinaDeEstados(AMBOS_LADOS,waitNORTE));
}
TEST(State7, Machine7){
    ASSERT_EQ(goESTE, MaquinaDeEstados(AMBOS_LADOS,waitNORTE));
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
